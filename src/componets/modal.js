import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import CloseIcon from './close-icon';
import { Heading } from './fonts';
const Overlay = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 2;
`;

const Body = styled.div`
  background-color: white;
  position: relative;
  overflow-y: auto;
  width: ${props => props.width || 420}px;
  height: ${props => props.height || 420}px;
`;
const ModalTitle = styled(Heading)`
  padding: 10px 0px 0px 16px;
`;
const Modal = ({ isShowing, hide, children, height, width, title }) =>
  isShowing
    ? ReactDOM.createPortal(
        <React.Fragment>
          <Overlay>
            <Body height={height} width={width}>
              <ModalTitle>{title}</ModalTitle>
              <hr />
              <CloseIcon onClick={() => hide()} />
              {children}
            </Body>
          </Overlay>
        </React.Fragment>,
        document.querySelector('#root'),
      )
    : null;
export default Modal;
