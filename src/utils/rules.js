export const rules = {
  RRD4D32: {
    conditions: {
      all: [
        {
          fact: 'amount',
          operator: 'greaterThanInclusive',
          value: 1000,
        },
      ],
    },
    event: {
      type: 'pre-discount',
      params: {
        message: '10',
      },
    },
  },
  '44F4T11': {
    conditions: {
      all: [
        {
          fact: 'amount',
          operator: 'greaterThanInclusive',
          value: 1500,
        },
      ],
    },
    event: {
      type: 'pre-discount',
      params: {
        message: '15',
      },
    },
  },
  FF9543D1: {
    conditions: {
      all: [
        {
          fact: 'docgenQty',
          operator: 'greaterThanInclusive',
          value: 10,
        },
      ],
    },
    event: {
      type: 'docgenPrice',
      params: {
        message: '8.99',
      },
    },
  },
  YYGWKJD: {
    conditions: {
      all: [
        {
          fact: 'wfQty',
          operator: 'greaterThanInclusive',
          value: 1,
        },
      ],
    },
    event: {
      type: 'formPrice',
      params: {
        message: '89.99',
      },
    },
  },
};
