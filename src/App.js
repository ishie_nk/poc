import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import styled, { ThemeProvider } from 'styled-components';
import { lightTheme } from './theme/theme';
import Header from './componets/header';
import allReducers from './allReducers';
import ProductList from './modules/product/ProductList';

const store = createStore(allReducers, applyMiddleware(thunk));

const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  background-color: ${props => props.theme.backgroundColor};
`;

const App = () => {
  // const [cartItems, addToCart] = React.useState([]);
  return (
    <Provider store={store}>
      <ThemeProvider theme={lightTheme}>
        <Wrapper>
          <Header />
          <ProductList />
        </Wrapper>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
