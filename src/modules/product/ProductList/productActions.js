export const addToCart = item => {
  return { type: 'ADD_TO_CART', payload: item, add: true };
};

export const removeFromCart = item => {
  return { type: 'ADD_TO_CART', payload: item, add: false };
};

export const addAllProduct = products => {
  return { type: 'ADD_ALL_PRODUCT', payload: products };
};
