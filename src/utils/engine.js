import { Engine } from 'json-rules-engine';
import { rules } from './rules';

const engine = new Engine();
engine.addRule(rules['44F4T11']);
engine.addRule(rules.FF9543D1);
engine.addRule(rules.RRD4D32);
engine.addRule(rules.YYGWKJD);

const calculateDiscountedPrice = async facts => {
  return Promise.resolve(engine.run(facts));
};

export default calculateDiscountedPrice;
