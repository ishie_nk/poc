import React from 'react';
import styled from 'styled-components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ProductItem from '../product-item';
import allProducts from '../../../assets/products.json';
import { addToCart, addAllProduct, removeFromCart } from './productActions';

export const List = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex: 1;
  align-items: center;
  margin: 0px 20px;
`;

const ProductList = ({ actions, products }) => {
  React.useEffect(() => {
    actions.addAllProduct(allProducts);
  }, [actions]);

  return (
    <List>
      {products &&
        products.map((item, index) => {
          return (
            <ProductItem
              key={index}
              added={item.QUANTITY}
              product={item}
              onClick={add => {
                add ? actions.addToCart(item) : actions.removeFromCart(item);
              }}
            />
          );
        })}
    </List>
  );
};

const mapStateToProps = state => {
  return {
    products: state.productReducer.products,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      { addToCart, addAllProduct, removeFromCart },
      dispatch,
    ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductList);
