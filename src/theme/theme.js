export const lightTheme = {
  backgroundColor: 'white',
  primaryColor: '#03a9f4',
  accentColor: '#ff3d00',
  primaryTextColor: 'black',
  secondaryTextColor: '#607d8b',
};
