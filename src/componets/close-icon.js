import styled from 'styled-components';

const CloseIcon = styled.a`
  position: absolute;
  right: 10px;
  top: 10px;
  width: 24px;
  height: 24px;
  border-radius: 50%;
  opacity: 0.5;
  :hover {
    opacity: 1;
  }
  ::before,
  ::after {
    position: absolute;
    left: 10px;
    content: ' ';
    height: 24px;
    width: 2px;
    background-color: #333;
  }
  ::before {
    transform: rotate(45deg);
  }
  ::after {
    transform: rotate(-45deg);
  }
`;
export default CloseIcon;
