import { combineReducers } from 'redux';
import productReducer from './modules/product/ProductList/productReducer';

const rootReducer = combineReducers({ productReducer });

export default rootReducer;
