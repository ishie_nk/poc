import React from 'react';
import { CartIcon } from '../assets/icon';
import styled from 'styled-components';

const Icon = styled(CartIcon)`
  height: 25px;
  width: 25px;
  margin-right: 10px;
`;

const Badge = styled.span`
  border-radius: 50%;
  border-width: 10px;
  height: 14px;
  width: 14px;
  background-color: ${props => props.theme.accentColor};
  padding: 2px;
  font-size: 10px;
  color: white;
  text-align: center;
  font-family: sans-serif;
  position: absolute;
  top: -6px;
  right: 6px;
`;

const Cart = ({ onClick, numberOfItems }) => (
  <div onClick={onClick} style={{ position: 'relative' }}>
    <Icon />
    <Badge>{numberOfItems}</Badge>
  </div>
);

export default Cart;
