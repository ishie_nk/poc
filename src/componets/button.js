import styled from 'styled-components';
const Button = styled.button`
  background-color: ${props => props.theme.accentColor};
  padding: 4px 0px;
  color: #fff;
  border-radius: 4px;
`;
export default Button;
