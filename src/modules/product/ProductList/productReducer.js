const initialState = { cartItems: [], products: [] };

export default (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_ALL_PRODUCT':
      return {
        ...state,
        products: [...action.payload],
      };
    case 'ADD_TO_CART':
      let currentState = Object.assign({}, state);
      const products = [...state.products];
      const modifiedProducts = products.map(item =>
        item.PRODUCT_ID === action.payload.PRODUCT_ID
          ? {
              ...item,
              QUANTITY: action.add ? item.QUANTITY + 1 : item.QUANTITY - 1,
            }
          : item,
      );
      let items = [...currentState.cartItems];
      if (
        items.findIndex(
          item => item.PRODUCT_ID === action.payload.PRODUCT_ID,
        ) === -1
      ) {
        const newItem = Object.assign({}, action.payload);
        newItem.QUANTITY = newItem.QUANTITY + 1;
        items.push(newItem);
      } else {
        items = currentState.cartItems.map(item => {
          if (item.PRODUCT_ID === action.payload.PRODUCT_ID) {
            action.add ? item.QUANTITY++ : item.QUANTITY--;
          }
          return item;
        });
      }
      return {
        ...state,
        cartItems: items.filter(item => item.QUANTITY > 0),
        products: modifiedProducts,
      };
    default:
      return state;
  }
};
