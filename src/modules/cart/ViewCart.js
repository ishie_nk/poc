import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styled from 'styled-components';
import {
  addToCart,
  removeFromCart,
} from '../product/ProductList/productActions';
import ProductItem from '../product/product-item';
import { List } from '../product/ProductList';
import Button from '../../componets/button';
import { Heading, Description } from '../../componets/fonts';
import { rules } from '../../utils/rules';
import calculateDiscountedPrice from '../../utils/engine';

const CartWrapper = styled.div``;

const CheckoutWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 16px;
`;

const CheckoutButton = styled(Button)`
  padding: 8px;
  font-size: 16px;
  margin-bottom: 16px;
`;

const ApplyButton = styled(Button)`
  margin-top: 8px;
  margin-bottom: 8px;
  padding: 8px;
`;

const PromoInput = styled.input`
  margin-right: 16px;
  margin-top: 8px;
  margin-bottom: 8px;
`;

const computePrice = cartItems => {
  const amount = cartItems.reduce(
    (total, item) =>
      total +
      parseInt(item.PRICE.length ? item.PRICE.substr(1) : 0) * item.QUANTITY,
    0,
  );
  console.log(amount);
  return amount;
};

const applyPromo = async (promo, amount) => {
  let keys = Object.keys(rules);
  if (keys.findIndex(item => item === promo) === -1) {
    alert('No such promo found. Please retry.');
    return;
  }
  let facts = {
    amount,
    docgenQty: 0,
    wfQty: 0,
  };
  return Promise.resolve(
    calculateDiscountedPrice(facts).then(events => {
      return amount - (amount * parseInt(events[0].params.message)) / 100;
    }),
  );
};

const ViewCart = ({ actions, cartItems }) => {
  const [promo, setPromo] = React.useState('');
  const [price, setPrice] = React.useState(0);
  return cartItems && cartItems.length > 0 ? (
    <CartWrapper>
      <List>
        {cartItems.map((item, index) => {
          return (
            <ProductItem
              key={index}
              added={item.QUANTITY}
              product={item}
              onClick={add => {
                add ? actions.addToCart(item) : actions.removeFromCart(item);
              }}
            />
          );
        })}
      </List>
      <CheckoutWrapper>
        <CartWrapper style={{ display: 'flex', flexDirection: 'row' }}>
          <Description>Apply promo code:&nbsp;&nbsp;&nbsp;</Description>
          <PromoInput
            onChange={event => {
              setPromo(event.target.value);
            }}
            type="text"
            name="promo"
          />
          <ApplyButton
            onClick={async () =>
              setPrice(await applyPromo(promo, computePrice(cartItems)))
            }
          >
            Apply
          </ApplyButton>
        </CartWrapper>
      </CheckoutWrapper>
      <CheckoutWrapper style={{ marginTop: -16 }}>
        <Description>
          Cart Total: ${price || computePrice(cartItems)}
        </Description>
        <CheckoutButton>Checkout</CheckoutButton>
      </CheckoutWrapper>
    </CartWrapper>
  ) : (
    <Heading
      style={{
        textAlign: 'center',
        marginTop: `${window.screen.height / 6}px`,
      }}
    >
      NO ITEM AVAILABLE IN CART
    </Heading>
  );
};

const mapStateToProps = state => {
  return {
    cartItems: state.productReducer.cartItems,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ addToCart, removeFromCart }, dispatch),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ViewCart);
