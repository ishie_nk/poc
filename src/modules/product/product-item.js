import React from 'react';
import styled from 'styled-components';
import {
  Heading,
  Amount,
  Description,
  AddRemoveText,
} from '../../componets/fonts';
import Button from '../../componets/button';

const Product = styled.div`
  display: flex;
  height: 200px;
  width: 200px;
  flex-direction: column;
  border-radius: 4px;
  border: 1px solid transparent;
  background-color: white;
  margin: 12px;
  padding: 8px;
  filter: drop-shadow(1px 1px 3px ${props => props.theme.secondaryColor});
`;

const Header = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const Image = styled.img`
  width: 195px;
  height: 80px;
`;

const AddRemove = styled.div`
  flex-direction: 'row';
  display: flex;
  border-radius: 3px;
  border: 1px solid green;
`;

const ProductItem = ({ added, product, onClick, className }) => (
  <Product className={className}>
    <Image src={require('../../assets/images/imagePlaceholder.jpg')} />
    <Header>
      <Heading>{product.PRODUCT_NAME}</Heading>
      <Amount>{product.PRICE}</Amount>
    </Header>
    <Header>
      <Description>{product.DESCRIPTION}</Description>
      {added ? (
        <AddRemove>
          <AddRemoveText onClick={() => onClick(false)}>-</AddRemoveText>
          <AddRemoveText>{added}</AddRemoveText>
          <AddRemoveText onClick={() => onClick(true)}>+</AddRemoveText>
        </AddRemove>
      ) : (
        <Button onClick={() => onClick(true)}>Add to cart</Button>
      )}
    </Header>
  </Product>
);

export default ProductItem;
