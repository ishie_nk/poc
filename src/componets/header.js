import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import logo from '../logo.svg';
import { Settings } from '../assets/icon';
import Cart from './cart';
import useModal from './useModal';
import Modal from './modal';
import ViewCart from '../modules/cart/ViewCart';

const HeaderContainer = styled.div`
  height: 50px;
  display: flex;
  align-items: center;
  background-color: ${props => props.theme.primaryColor};
  justify-content: space-between;
  padding-right: 20px;
  padding-left: 10px;
`;

const IconContainer = styled.div`
  align-items: center;
  display: flex;
`;

const StyledSettings = styled(Settings)`
  height: 20px;
  width: 20px;
  margin-left: 20px;
  path {
    fill: ${props => props.theme.backgroundColor};
  }
`;
const Header = ({ settingClick, quantity }) => {
  const { isShowing, toggle } = useModal();

  return (
    <HeaderContainer>
      <Modal
        isShowing={isShowing}
        hide={toggle}
        width={800}
        height={window.screen.height / 2}
        title="Cart Items"
      >
        <ViewCart />
      </Modal>
      <IconContainer>
        <img
          src={logo}
          className="App-logo"
          alt="logo"
          style={{ width: '40px', height: '40px' }}
        />
        <label style={{ color: 'white' }}>POC</label>
      </IconContainer>
      <IconContainer>
        <Cart onClick={() => toggle()} numberOfItems={quantity} />
        <StyledSettings />
      </IconContainer>
    </HeaderContainer>
  );
};
const mapStateToProps = state => {
  const cartItems = state.productReducer.cartItems;
  return {
    quantity: cartItems.reduce((total, item) => total + item.QUANTITY, 0),
  };
};

export default connect(
  mapStateToProps,
  {},
)(Header);
