import styled from 'styled-components';

export const Heading = styled.h2`
  font-family: Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
  margin-bottom: 0;
  margin-top: 0;
`;

export const Amount = styled.h4`
  font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande',
    'Lucida Sans', Arial, sans-serif;
  font-weight: normal;
`;

export const Description = styled.p`
  font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande',
    'Lucida Sans', Arial, sans-serif;
`;

export const AddRemoveText = styled.b`
  color: green;
  background-color: azure;
  font-size: 12px;
  padding: 8px;
  cursor: pointer;
`;
